
public class Dash extends CheckingUnit implements State {

	@Override
	public void handleChar(char word) {
		
		if((word=='e'||word=='E')&&WordCounter.havingFirstE==false)
		{
			WordCounter.havingFirstE = true;
			WordCounter.state = new E_first();
		}
		else if(super.isVowel(word)||word=='y'||word=='Y')
		{
			WordCounter.vowelFound = true;
			WordCounter.syllables+=1;
			WordCounter.state = new Vowel();
		}
		else if(super.isLetter(word))
		{
			
			WordCounter.state = new Consonant();
		}
		else
		{
			WordCounter.state = new NonWord();
		}
		
	}
	
	

}
