
public class NonWord extends CheckingUnit implements State{

	@Override
	public void handleChar(char word) {
		
		WordCounter.havingFirstE = false;
		
		if(word=='e'||word=='E'&&WordCounter.havingFirstE==false)
		{
			WordCounter.syllables+=1;
			WordCounter.havingFirstE = true;
			WordCounter.state = new E_first();
		}
		else if(super.isVowel(word))
		{
			WordCounter.syllables++;
			WordCounter.state = new Vowel();
		}
		else if(super.isLetter(word))
		{
			WordCounter.state = new Consonant();
		}
		else
		{
			WordCounter.state = new NonWord();
		}
		
	
	}

}
