
public class E_first extends CheckingUnit implements State{

	@Override
	public void handleChar(char word) {
		

		if(super.isVowel(word))
		{
				WordCounter.syllables++;
				WordCounter.state = new Vowel();
		}
		else if(super.isLetter(word))
		{
			WordCounter.state = new Consonant();
		}
		else
		{
			WordCounter.state = new NonWord();
		}
	}


}
